<?php

namespace App\Http\Controllers;

use DB;
use Uuid;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Data\DataCuttingDev;
use App\Http\Controllers\Controller;

class MasterDataController extends Controller
{
    public function index()
    {
        return view('master_data.index');
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = DB::table('jz_data_cutting')->orderBy('cutting_date', 'desc');

            return datatables()->of($data)
            ->editColumn('cutting_date', function($data) {
                return Carbon::parse($data->cutting_date)->format('d-M-Y');
            })
            ->editColumn('new_size', function($data) {
                if ($data->new_size == 'J') return 'Japan';
                elseif ($data->new_size == 'A') return 'Asia';
                else return '-';
            })
            ->addColumn('action', function($data) {
                return view('master_data._action', [
                    'model'      => $data,
                    'detail'     => route('masterData.show',$data->style)
                ]);
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }
}
