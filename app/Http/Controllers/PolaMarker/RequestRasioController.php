<?php

namespace App\Http\Controllers\PolaMarker;

use DB;
use Uuid;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Data\DataCuttingDev;
use App\Http\Controllers\Controller;

class RequestRasioController extends Controller
{
    public function index()
    {
        return view('pola_marker.request_rasio.index');
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = DB::table('jz_data_cutting')->orderBy('cutting_date', 'desc');

            return datatables()->of($data)
            ->editColumn('cutting_date', function($data) {
                return Carbon::parse($data->cutting_date)->format('d-M-Y');
            })
            ->editColumn('new_size', function($data) {
                if ($data->new_size == 'J') return 'Japan';
                elseif ($data->new_size == 'A') return 'Asia';
                else return '-';
            })
            ->addColumn('action', function($data) {
                return view('master_data._action', [
                    'model'      => $data,
                    'detail'     => route('masterData.show',$data->style)
                ]);
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function download(Request $request)
    {
        $this->validate($request, [
            'cutting_date' => 'required',
        ]);

        $cutting_date = $request->cutting_date;

        $data = DB::table('jz_data_cutting')->where('cutting_date', $cutting_date)
        ->orderBy('style', 'desc');

        if(count($data->get()) < 1)
        {
            // return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'AOI_2_Excel_SSP_'.$cutting_date;
            
            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'order_tp', 'Customer', 'LC Date', 'Season', 'order_no', 'style_no', 'article_no', 'article_name', 'Cutting Date', 'Garment Type', 'Unit', 'order_qty', 'size_ctn',
                        'ts_01', 'qty_01',
                        'ts_02', 'qty_02',
                        'ts_03', 'qty_03',
                        'ts_04', 'qty_04',
                        'ts_05', 'qty_05',
                        'ts_06', 'qty_06',
                        'ts_07', 'qty_07',
                        'ts_08', 'qty_08',
                        'ts_09', 'qty_09',
                        'ts_10', 'qty_10',
                        'ts_11', 'qty_11',
                        'ts_12', 'qty_12',
                        'ts_13', 'qty_13',
                        'ts_14', 'qty_14',
                        'ts_15', 'qty_15',
                        'ts_16', 'qty_16',
                        'ts_17', 'qty_17',
                        'ts_18', 'qty_18',
                        'ts_19', 'qty_19',
                        'ts_20', 'qty_20',
                        'ts_21', 'qty_21',
                        'ts_22', 'qty_22',
                        'ts_23', 'qty_23',
                        'ts_24', 'qty_24',
                        'ts_25', 'qty_25',
                        'ts_26', 'qty_26',
                        'part_no', 'YY MO', 'usage_', 'matr_tp', 'matr_cd', 'Combine', 'Width', 'Season2', 'FabQty', 'Max lay', 'Min lay', 'Max Length', 'End loss', 'piping', 'tgl info', 'jam info', 'actual width', 'actual qty', 'keterangan'
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            
                            $data_detail = DataCuttingDev::where('style', $row->style)
                                                        ->where('articleno', $row->articleno)
                                                        ->where('part_no', '10');
                            if($row->new_size == 'J') {
                                $data_detail = $data_detail->where('destination', 'LIKE', '%Japan%');
                            } else {
                                $data_detail = $data_detail->where('destination', 'NOT LIKE', '%Japan%');
                            }

                            $data_detail = $data_detail->get()->first();

                            $data_detail_size = DB::table('jz_detail_size')
                                                    ->where('style', $row->style)
                                                    ->where('articleno', $row->articleno)
                                                    ->where('new_size', $row->new_size)
                                                    ->where('cutting_date', $row->cutting_date)
                                                    ->get();

                            $data_excel = array(
                                'INLINE',
                                $data_detail->customer,
                                $data_detail->lc_date,
                                $data_detail->season,
                                $data_detail->style.'-'.$data_detail->po_buyer.'-'.$data_detail->articleno,
                                $row->style,
                                $row->articleno,
                                '',
                                $data_detail->cutting_date,
                                '',
                                'PCS',
                                $row->total_qty,
                                ''
                            );

                            foreach($data_detail_size as $sz) {
                                $data_excel[] = $sz->size_finish_good;
                                $data_excel[] = $sz->total_qty;
                            }

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            //return response()->json(200);   
        }
    }
}
