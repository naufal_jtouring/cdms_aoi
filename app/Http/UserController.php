<?php namespace App\Http\Controllers;

use DB;
use File;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $roles = Role::pluck('display_name', 'id')->all();
        return view('user.index',compact('roles'));
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = User::orderby('created_at','desc');
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('user._action', [
                    'model' => $data,
                    'edit_modal' => route('user.edit',$data->id),
                    'delete' => route('user.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }

    public function store(Request $request)
    {
        $avatar = Config::get('storage.avatar');
        if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required',
            'sex' => 'required',
        ]);

        if(User::where('email',$request->email)->exists()) return response()->json('Email sudah ada, silahkan cari nama Email lain.', 422);

        if($request->nik)
        {
            if(User::where('nik',$request->nik)->exists()) 
                return response()->json('Nik sudah ada.', 422);
        }

        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $file = $request->file('photo');
                $now = Carbon::now()->format('u');
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height = $image_resize->height();
                $width = $image_resize->width();

                $newWidth = 130;
                $newHeight = 130;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        $user = User::firstorCreate([
            'name' => $request->name,
            'nik' => $request->nik,
            'sex' => $request->sex,
            'photo' => $image,
            'email' => $request->email,
            'email_verified_at' => carbon::now(),
            'password' => bcrypt($request->password)
        ]);

        $mappings =  json_decode($request->mappings);
        $array = array();

        foreach ($mappings as $key => $mapping) $array [] = [ 'id' => $mapping->id ];
        
        if($user->save()) $user->attachRoles($array);
        return response()->json(200);
    }

    public function storeRole(Request $request)
    {
        $user = User::find($request->user_id);
        $user->attachRoles([$request->role_id]);
        return response()->json(200);
    }

    public function edit($id)
    {
        $user = User::find($id);
        $obj = new StdClass();
        $obj->id = $id;
        $obj->name = $user->name;
		$obj->nik = $user->nik;
		$obj->email = $user->email;
		$obj->sex = $user->sex;
		$obj->permissions = $user->roles()->get();
		$obj->url_update = route('user.update',$user->id);
		$obj->url_role_user = route('user.dataRole',$user->id);
		
		return response()->json($obj,200);
    }

    public function dataRole(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $data = db::select(db::raw("select roles.id as id
            ,roles.display_name
            from roles 
            join role_user on role_user.role_id = roles.id 
            where role_user.user_id = '".$id."'
            "));

            return datatables()->of($data)
            ->addColumn('action', function($data)use($id){
                return view('role._action_modal', [
                    'model' => $data,
                    'delete' => route('user.destroyRoleUser',[$id,($data)?$data->id : null]),
                ]);

               
            })
            ->make(true);
        }
        
    }

    public function update(Request $request, $id)
    {
        $avatar = Config::get('storage.avatar');
        if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required',
            'sex' => 'required',
        ]);
        
        if(User::where([
            ['email',$request->email],
            ['id','!=',$id],
        ])->exists()) 
            return response()->json('Email sudah ada, silahkan cari nama Email lain.', 422);

        if($request->nik)
        {
            if(User::where([
                ['nik',$request->nik],
                ['id','!=',$id],
            ])->exists()) 
                return response()->json('Nik sudah ada.', 422);
        }

        $user = User::find($id);
        $image = null;
        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $old_file = $avatar . '/' . $user->photo;
                if(File::exists($avatar)) File::delete($old_file);

                $file = $request->file('photo');
                $now = Carbon::now()->format('u');
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height = $image_resize->height();
                $width = $image_resize->width();

                $newWidth = 130;
                $newHeight = 130;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        $user->nik = $request->nik;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->sex = $request->sex;
        if ($request->password) $user->password = bcrypt($request->password);
        if ($image) $user->photo = $image;
        $user->save();

        return response()->json(200);
    }

   
    public function destroy($id)
    {
        $user = User::findorFail($id)->delete();
        return response()->json(200);
    }

    public function destroyRoleUser($user_id,$role_id)
    {
        $user = User::find($user_id);
        $roles = $user->roles()->where('role_id','!=',$role_id)->get();
        $array = array();
        foreach ($roles as $key => $role) {
            $array [] = $role->id;
        }
        
        $user->roles()->sync([]);
        $user->attachRoles($array);

        return response()->json($roles);
    }
}
