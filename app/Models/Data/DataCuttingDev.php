<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;

class DataCuttingDev extends Model
{
    // use Uuids;
    // public $incrementing    = false;
    // protected $connection   = 'erp_dev';
    protected $table        = 'data_cuttings';
    protected $guarded      = ['id'];
    protected $dates        = ['created_at', 'updated_at'];
    protected $fillable     = [
        'documentno',
        'style',
        'job_no',
        'po_buyer',
        'customer',
        'destination',
        'product',
        'ordered_qty',
        'part_no',
        'material',
        'color_name',
        'product_category',
        'cons',
        'fbc',
        'cutting_date',
        'uom',
        'is_piping',
        'custno',
        'statistical_date',
        'lc_date',
        'upc',
        'color_code_raw_material',
        'width_size',
        'code_category_raw_material',
        'desc_category_raw_material',
        'desc_produksi',
        'mo_updated',
        'ts_lc_date',
        'articleno',
        'size_finish_good',
        'size_category',
        'color_finish_good',
        'warehouse',
        'desc_mo',
        'status_ori',
        'mo_created',
        'promised_date',
        'season',
        'article_name',
        'garment_type',
        'deleted_at',
    ];
}
