@extends('layouts.app',['active' => 'marker_request_rasio'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Request Pola</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Request Rasio</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="{{ route('markerRequestRasio.download') }}" id="download_request_rasio" method="POST">
			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2 text-bold">Input Cutting Date</label>
					<div class="col-lg-10">
						@csrf
						<div class="input-group">
							<span class="input-group-btn">	
								<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
							</span>
							<input class="form-control" type="date" name="cutting_date" id="cutting_date">
							<span class="input-group-btn">
								<button class="btn btn-primary legitRipple" type="submit">Download</button>
							</span>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
@endsection

@section('page-js')
<script>
	// $(document).ready( function () {
	// 	$.ajaxSetup({
	// 		headers: {
	// 			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	// 		}
	// 	});
	// 	$('#download_request_rasio').submit(function (event){
	// 		event.preventDefault();
	// 		var cutting_date = $('#cutting_date').val();
			
	// 		if(!cutting_date){
	// 			$("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
	// 			return false;
	// 		}
			
	// 		$.ajax({
	// 			type: "POST",
	// 			url: $('#download_request_rasio').attr('action'),
	// 			data: {
	// 				cutting_date: cutting_date,
	// 			},
	// 			beforeSend: function () {
	// 				$.blockUI({
	// 					message: '<i class="icon-spinner4 spinner"></i>',
	// 					overlayCSS: {
	// 						backgroundColor: '#fff',
	// 						opacity: 0.8,
	// 						cursor: 'wait'
	// 					},
	// 					css: {
	// 						border: 0,
	// 						padding: 0,
	// 						backgroundColor: 'transparent'
	// 					}
	// 				});
	// 			},
	// 			complete: function () {
	// 				$.unblockUI();
	// 			},
	// 			success: function () {
	// 				$('#download_request_rasio').trigger("reset");
	// 				$("#alert_success").trigger("click", 'Data Berhasil didownload');
	// 			},
	// 			error: function (response) {
	// 				$.unblockUI();
					
	// 				if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
	// 			}
	// 		});
	// 	});
	// });
</script>
@endsection
