@extends('layouts.app',['active' => 'master_data'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Cutting (from ERP)</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="{{ route('purchaseOrder.index') }}">Master Data Cutting (from ERP)</a></li>
            <li class="active">Item</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<p>Purchase Date : <b>{{ $purchase_order->purchase_date->format('d/M/Y') }}</b></p>
					<p>Purchase Number :  <b>{{ strtoupper($purchase_order->purchase_number) }}</b></p>
					<p>Supplier Name :  <b>{{ strtoupper($purchase_order->supplier_name) }}</b></p>
				</div>
				<div class="col-md-4">
					<p>Factory Name :  <b>{{ strtoupper($purchase_order->factory_name) }}</b></p>
					<p>Description :  <b>{{ $purchase_order->description }}</b></p>
					<p>Document Status :  <b>{{ ucfirst($purchase_order->purchase_status_description) }} ({{strtoupper($purchase_order->purchase_status_code)}})</b></p>

				</div>
				<div class="col-md-4">
					<p>Payment Term :  <b>{{ strtoupper($purchase_order->payment_term) }}</b></p>
					<p>Currency : <b>{{ strtoupper($purchase_order->currency) }}</b></p> 
					<p>Total Price :  <b>{{ number_format($purchase_order->total_price, 2, '.', ',') }}</b></p>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-basic table-condensed" id="purchaseOrderDetailTable">
					<thead>
						<tr>
							<th>Id</th>
							<th>Item Code</th>
							<th>Item Name</th>
							<th>Category</th>
							<th>Uom</th>
							<th>Qty</th>
							<th>Price</th>
							<th>Total Price Per Item</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	{!! Form::hidden('purchase_order_id', $purchase_order->id, array('id' => 'purchase_order_id')) !!}
@endsection

@section('page-js')
<script>
	$(document).ready( function () 
	{
		var id = $('#purchase_order_id').val();
		$('#purchaseOrderDetailTable').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/purchase-order/detail/'+id+'/data',
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'item_code', name: 'item_code',searchable:true,orderable:true},
				{data: 'item_name', name: 'item_name',searchable:true,orderable:true},
				{data: 'category', name: 'category',searchable:true,orderable:true},
				{data: 'uom', name: 'uom',searchable:true,orderable:true},
				{data: 'qty', name: 'qty',searchable:false,orderable:false},
				{data: 'price', name: 'price',searchable:false,orderable:false},
				{data: 'total_price_item', name: 'total_price_item',searchable:false,orderable:false}
			]
		});

		var dtable = $('#purchaseOrderDetailTable').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
		});
		dtable.draw();
	});
</script>
@endsection
