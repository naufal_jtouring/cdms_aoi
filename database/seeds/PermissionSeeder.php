<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'menu-user',
                'display_name' => 'menu user',
                'description' => 'menu user'
            ],
            [
                'name' => 'menu-role',
                'display_name' => 'menu role',
                'description' => 'menu role'
            ],
            [
                'name' => 'menu-permission',
                'display_name' => 'menu permission',
                'description' => 'menu permission'
            ],
            [
                'name' => 'menu-master-data',
                'display_name' => 'menu master data',
                'description' => 'menu master data'
            ],
            [
                'name' => 'menu-cutting-preparation',
                'display_name' => 'menu cutting preparation',
                'description' => 'menu cutting preparation'
            ],
            [
                'name' => 'menu-marker-request-rasio',
                'display_name' => 'menu marker request rasio',
                'description' => 'menu marker request rasio'
            ],
        ];

        foreach ($permissions as $key => $permission) {
            Permission::create([
                'name' => $permission['name'],
                'display_name' => $permission['display_name'],
                'description' => $permission['description']
            ]);
        }
    }
}
