<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuttingPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cutting_plans', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->date('cutting_date');
            $table->string('style');
            $table->string('articleno');
            $table->integer('queu')->nullable();
            $table->integer('user_id');
            $table->timestamps();
            $table->timestamp('delete_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cutting_plans');
    }
}
