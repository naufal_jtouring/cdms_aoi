<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimulasiDataERPsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simulasi_data_e_r_ps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('documentno')->nullable();
            $table->string('style')->nullable();
            $table->string('job_no')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('customer')->nullable();
            $table->string('dest')->nullable();
            $table->string('product')->nullable();
            $table->integer('qtyordered')->nullable();
            $table->string('part_no')->nullable();
            $table->string('material')->nullable();
            $table->string('color')->nullable();
            $table->string('product_category')->nullable();
            $table->double('cons', 15, 8)->nullable();
            $table->double('fbc', 15, 8)->nullable();
            $table->date('datestartschedule')->nullable();
            $table->string('uom')->nullable();
            $table->string('ispiping')->nullable();
            $table->string('custno')->nullable();
            $table->date('kst_statisticaldate')->nullable();
            $table->date('kst_lcdate')->nullable();
            $table->bigInteger('upc')->nullable();
            $table->string('color_code_raw_material')->nullable();
            $table->integer('width_size')->nullable();
            $table->string('code_raw_material')->nullable();
            $table->text('desc_raw_material')->nullable();
            $table->text('desc_produksi')->nullable();
            $table->date('updated')->nullable();
            $table->date('kst_tslc')->nullable();
            $table->string('kst_articleno')->nullable();
            $table->string('size_fg')->nullable();
            $table->string('color_finish_good')->nullable();
            $table->string('m_warehouse_id')->nullable();
            $table->date('description_mo')->nullable();
            $table->string('status_ori')->nullable();
            $table->date('created')->nullable();
            $table->date('datepromised')->nullable();
            $table->string('season')->nullable();
            $table->string('article_name')->nullable();
            $table->string('garment_type')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simulasi_data_e_r_ps');
    }
}
