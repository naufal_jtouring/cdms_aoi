const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
.scripts([
   'resources/js/mustache.js',
   'resources/js/plugins/loaders/pace.min.js',
   'resources/js/core/libraries/jquery.min.js',
   'resources/js/core/libraries/bootstrap.min.js',
   'resources/js/plugins/loaders/blockui.min.js',
   'resources/js/core/libraries/jquery_ui/interactions.min.js',
   'resources/js/plugins/forms/selects/select2.min.js',
   'resources/js/plugins/forms/styling/uniform.min.js',
   'resources/js/plugins/ui/moment/moment.min.js',
   'resources/js/plugins/ui/fullcalendar/fullcalendar.min.js',
   'resources/js/core/app.js',
   'resources/js/plugins/ui/ripple.min.js',
   'resources/js/plugins/tables/datatables/datatables.min.js',
   'resources/js/plugins/tables/datatables/extensions/responsive.min.js',
 ],'public/js/backend.js')
 .scripts([
   'resources/js/plugins/pickers/daterangepicker.js',
 ],'public/js/datepicker.js')
 .scripts([
   'resources/js/pages/role.js',
 ],'public/js/role.js')
 .scripts([
  'resources/js/pages/user.js',
],'public/js/user.js')
 .scripts([
    "resources/js/plugins/notifications/bootbox.min.js",
    "resources/js/plugins/notifications/sweet_alert.min.js",
    "resources/js/pages/notification.js",
  ],"public/js/notification.js")
  .styles([
   'resources/css/bootstrap.css',
   'resources/css/core.css',
   'resources/css/components.css',
   'resources/css/colors.css',
   'resources/css/custom.css'
],'public/css/backend.css')
.copyDirectory('resources/images', 'public/images')
.copyDirectory('resources/css/icons','public/css/icons')
.sass('resources/sass/app.scss', 'public/css');